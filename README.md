# InvTracker

**Evan Young June 2020**

Pathfinder RPG Adventure Company Inventory Tracker Python, Flask Rest API Backend with SQL Alchemy, I think it will have a Flask front end as well. 

This is my attempt to break out of Python tutorial hell and get real software working.

Currently working from: https://www.freecodecamp.org/news/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563/