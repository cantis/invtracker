import datetime

from app.main import db
from app.main.model.player import Player


def save_new_player(data):
    # Check and see if player exists
    player = Player.query.filter_by(email=data['email']).first()

    # Doesn't exist, create it
    if not player:
        new_player = Player(
            email=data['email'],
            username=data['username'],
            password=data['password'],
            registered_on=datetime.datetime.utcnow(),
            firstName=data['firstName'],
            lastName=data['lastName'],
            isActive=True
        )
        save_changes(new_player)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered'
        }
        return response_object, 201
    else:
        response_object = {
            'status': 'fail',
            'message': 'Player already exists. Please log in'
        }
        return response_object, 409


def get_all_players():
    players = Player.query.all()
    return players


def get_a_player(id):
    player = Player.query.filter_by(id=id).first()
    return player


def save_changes(data):
    db.session.add(data)
    db.session.commit()
