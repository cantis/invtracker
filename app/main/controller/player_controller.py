from flask import request
from flask_restplus import Resource

from ..util.dto import PlayerDto
from ..service.player_service import save_new_player, get_a_player, get_all_players

api = PlayerDto.api
_player = PlayerDto.player


@api.route('/')
class PlayerList(Resource):
    @api.doc('List_of_registered_players')
    @api.marshal_list_with(_player, envelope='data')
    def get(self):
        """List of all registered players"""
        return get_all_players()

    @api.response(201, "Player Successfully Created")
    @api.doc('create new player')
    @api.expect(_player, validate=True)
    def post(self):
        """Creates new player"""
        data = request.json
        return save_new_player(data=data)


@api.route('/<id>')
@api.param('id', 'The Player Id')
@api.response(404, 'Player not found.')
class Player(Resource):
    @api.doc('get a user')
    @api.marshal_with(_player)
    def get(self, id):
        """Gets a player, given its id"""
        player = get_a_player(id)
        if not player:
            api.abort(404)
        else:
            return player
