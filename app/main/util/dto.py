from flask_restplus import Namespace, fields


class PlayerDto:
    api = Namespace('player', description='player related operations')
    player = api.model('player', {
            'email': fields.String(required=True, description='player email address'),
            'username': fields.String(required=True, description='player user name'),
            'password': fields.String(required=True, description='player password'),
            'firstName': fields.String(required=True, description='first name'),
            'lastName': fields.String(requierd=True, description='last name')
    })
