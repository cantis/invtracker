import unittest
import datetime

from app.main import db
from app.main.model.player import Player
from app.test.base import BaseTestCase


class TestPlayerModel(BaseTestCase):

    def test_encode_auth_token(self):
        # arrange
        player = Player(
            firstName='Adam',
            lastName='Alpha',
            isActive=True,
            email='test@noplace.com',
            password='test',
            registered_on=datetime.datetime.utcnow(),
            username='testUser'
        )
        # act
        db.session.add(player)
        db.session.commit()
        auth_token = player.encode_auth_token(player.id)
        # assert
        self.assertTrue(isinstance(auth_token, bytes))

    def test_decode_auth_token(self):
        # arrange
        player = Player(
            firstName='Adam',
            lastName='Alpha',
            isActive=True,
            email='test@noplace.com',
            password='test',
            registered_on=datetime.datetime.utcnow(),
            username='testUser'
        )
        # act
        db.session.add(player)
        db.session.commit()
        auth_token = player.encode_auth_token(player.id)
        # assert
        self.assertTrue(isinstance(auth_token, bytes))
        self.assertTrue(Player.decode_auth_token(auth_token.decode("utf-8")) == 1)


if __name__ == '__main__':
    unittest.main()
