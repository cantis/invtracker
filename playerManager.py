# from persist import Player, PlayerSchema, db
# from flask import request, jsonify, Blueprint

# player_api = Blueprint("player_api", __name__)


# # Add New Player
# @player_api.route('/api/player', methods=['POST'])
# def add_player():
#     newPlayer = Player(request.json['firstName'], request.json['lastName'], True)
#     db.session.add(newPlayer)
#     db.session.commit()
#     return player_schema.jsonify(newPlayer)


# # Get All Players
# @player_api.route('/api/player', methods=['GET'])
# def get_players():
#     try:
#         allPlayers = Player.query.all()
#         result = players_schema.dump(allPlayers)
#         return jsonify(result)
#     except Exception as ex:
#         return jsonify(ex)


# # Get A Player
# @player_api.route('/api/player/<id>', methods=['GET'])
# def get_player(id):
#     player = Player.query.get(id)
#     result = player_schema.dump(player)
#     return jsonify(result)


# # Update a Player
# @player_api.route('/api/player/<id>', methods=['PUT'])
# def update_player(id):
#     player = Player.query.get(id)
#     player.firstName = request.json['firstName']
#     player.lastName = request.json['lastName']
#     player.isActive = request.json['isActive']
#     db.session.commit()
#     return player_schema.jsonify(player)


# # Delete a Player (mark as inactive)
# @player_api.route('/api/player/<id>', methods=['DELETE'])
# def delete_player(id):
#     player = Player.query.get(id)
#     # player.isActive = False
#     db.session.delete(player)
#     db.session.commit()
#     return player_schema.jsonify(player)


# # Init Schema
# player_schema = PlayerSchema()
# players_schema = PlayerSchema(many=True)